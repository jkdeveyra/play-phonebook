import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "Play21-Slick-Demo"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "mysql" % "mysql-connector-java" % "5.1.18",
    "com.typesafe" % "slick_2.10.0-M7" % "0.11.1",
    "org.scalatest" % "scalatest_2.10.0-M7" % "2.0.M4-2.10.0-M7-B1" % "test",
    "org.scala-lang" % "scala-actors" % "2.10.0-M7" % "test"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
  )
}
