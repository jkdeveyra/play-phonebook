import org.scalatest.{WordSpec, FunSpec}
import org.scalatest.matchers.ShouldMatchers

// Use MySQLDriver to connect to an H2 database
import scala.slick.driver.MySQLDriver.simple._

// Use the implicit threadLocalSession
import Database.threadLocalSession

import models._

class PhonebookSpecs extends WordSpec with ShouldMatchers {

  "Phonebook" should {

    "be createable" in {
      Database.forURL("jdbc:mysql://localhost/SlickDemo", driver= "com.mysql.jdbc.Driver") withSession {
        Phonebook.ddl.create
        Phonebook.insert(Person(None, "JK", "de Veyra", "123545"))

        val p = for (p <- Phonebook) yield p
        p.first.id.get should equal (1)

      }
    }
  }
}
