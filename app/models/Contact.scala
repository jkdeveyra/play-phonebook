package models

import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession

case class Contact(id: Option[Int], firstName: String, lastName: String, contactNumber: String) {

  def fullName = "%s, %s" format (lastName, firstName)

}

object Contacts extends Table[Contact]("Contact") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def firstName = column[String]("firstName")

  def lastName = column[String]("lastName")

  def contactNumber = column[String]("contactNumber")

  def * = id.? ~ firstName ~ lastName ~ contactNumber <> (Contact, Contact.unapply _)

  def findAll(implicit db: Database): List[Contact] = db withSession this.map(p => p).list

  def findById(id: Int)(implicit db: Database): Option[Contact] = db withSession {
    val list = this.map(p => p).filter(_.id === id).list

    if (list.isEmpty) None
    else Some(list.head)
  }

  def save(c: Contact)(implicit db: Database) = db withSession this.insert(c)

}
