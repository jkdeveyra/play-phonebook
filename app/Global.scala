import play.api.db.DB
import play.api.GlobalSettings

// Use H2Driver to connect to an H2 database

import scala.slick.driver.MySQLDriver.simple._

// Use the implicit threadLocalSession

import Database.threadLocalSession

import play.api.Application
import play.api.Play.current
import models._

object Global extends GlobalSettings {

  override def onStart(app: Application) {

    Application.db.withSession {
//      Contacts.ddl.create
    }
  }
}
