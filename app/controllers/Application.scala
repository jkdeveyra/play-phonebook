package controllers

import play.api._
import db.DB
import play.api.mvc._
import models._
import play.api.Play.current
import play.api.data.Forms._
import play.api.data.Form
import play.api.data._
import views._

// Use MySQLDriver to connect to a MySQL database
import scala.slick.driver.MySQLDriver.simple._

// Use the implicit threadLocalSession
import Database.threadLocalSession

import scala.collection.mutable.ListBuffer

object Application extends Controller {

  lazy implicit val db = Database.forDataSource(DB.getDataSource())

  val personForm = Form(
    mapping(
      "id" -> optional(number),
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText,
      "contactNumber" -> nonEmptyText
    )(Contact.apply)(Contact.unapply)
  )

  def index = Action { implicit req =>
    val list = Contacts.findAll
    Ok(html.index(list))
  }

  def view(id: Int) = Action { implicit req =>

    val contact = Contacts.findById(id)

    if (contact.isEmpty) {
      Redirect(routes.Application.index) flashing ("warning" -> ("User with id " + id + " does not exists"))
    } else
      Ok(html.view(contact.head))
  }

  def create = Action {
    Ok(html.create(personForm))
  }

  def save = Action { implicit req =>

    val form = personForm.bindFromRequest

    if (!form.hasErrors) {
      Contacts.save(form.get)
      Redirect(routes.Application.index)
    } else
      Ok(html.create(form))
  }
}